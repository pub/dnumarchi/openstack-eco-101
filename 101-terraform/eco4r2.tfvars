# =============================
# Variables pour Eco4 Région 2
# =============================

# Jeu de clés publiques ssh
vm_sshkeys = "my-keys"
# Zone DNS externe allouée
dns_zone_edd = "mon-projet.r2.eco4.cloud.e2.rie.gouv.fr."

# Identifiant du projet, préfixe des ressources
lab_id = "lab101"
# Réseau externe en sortie
net_egress = "net-egress"
# Réseau externe en entrée web
net_ingress = "net-ingress"

# Identifiants des gabarits de load balancer
lb_flavors = {
  "single" = "d2c4319c-16d3-453c-a9fa-e5a93955aab0"
  "ha"     = "fe1a0432-302d-4d09-85ec-ac38ecdb37d8"
}
