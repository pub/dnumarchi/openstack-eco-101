/*
  Variables projet
*/
variable "lab_id" {
  description = "Identifiant du projet, préfixe des ressources"
  type        = string
  nullable    = false
}

/*
  Variables machines virtuelles
*/
variable "vm_image" {
  description = "Image du système d'exploitation"
  type        = string
  default     = "Debian 12"
}

variable "vm_bastion_props" {
  description = "Propriétés de la VM bastion"
  type        = map(string)
  default = {
    name   = "bastion"
    image  = "Debian 12"
    flavor = "m1.tiny"
    ipv4   = "10.0.101.10"
  }
}

variable "vm_web_props" {
  description = "Propriétés de la VM web"
  type        = map(string)
  default = {
    name   = "web"
    image  = "Debian 12"
    flavor = "m1.small"
    ipv4   = "10.0.101.20"
  }
}

/*
  Variables de sécurité
*/
variable "vm_sshkeys" {
  description = "Jeu de clé SSH des VM"
  type        = string
  nullable    = false
}

/*
  Variables réseau
*/
variable "net_egress" {
  description = "Nom du réseau externe egress / sortie"
  type        = string
  nullable    = false
}

variable "net_ingress" {
  description = "Nom du réseau externe ingress / entrée web"
  type        = string
  nullable    = false
}

variable "subnet_cidr" {
  description = "Masque CIDR du sous-réseau"
  type        = string
  default     = "10.0.101.0/24"
}

variable "ip_router_internal" {
  description = "Adresse IP du routeur dans le projet"
  type        = string
  default     = "10.0.101.1"
}

variable "dns_servers" {
  description = "Liste de serveurs DNS"
  type        = list(string)
  default     = ["10.167.130.2"]
}

variable "dns_zone_edd" {
  description = "Nom de la zone DNS exposée allouée au projet"
  type        = string
  nullable    = false
}

/*
  Variables répartiteurs de charge
*/
variable "lb_flavors" {
  description = "Gabarits des LB"
  type        = map(string)
  nullable    = false
}
