# Le réseau externe ssh rie/edd
data "openstack_networking_network_v2" "net_egress" {
  name = var.net_egress
}

# Le réseau externe web rie/edd
data "openstack_networking_network_v2" "net_ingress_web" {
  name = var.net_ingress
}

# La zone DNS exposée allouée au projet
data "openstack_dns_zone_v2" "edd_zone" {
  name = var.dns_zone_edd
}
