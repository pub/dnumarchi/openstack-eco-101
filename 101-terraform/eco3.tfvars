# =============================
# Variables pour Eco4 Région 2
# =============================

# Jeu de clés publiques ssh
vm_sshkeys = "my-keys"
# Zone DNS externe allouée
dns_zone_edd = "mon-projet.r2.eco4.cloud.e2.rie.gouv.fr."

# Identifiant du projet, préfixe des ressources
lab_id = "lab101"
# Réseau externe en sortie
net_egress = "net-egress"
# Réseau externe en entrée web
net_ingress = "net-ingress"

# Identifiants des gabarits de load balancer
lb_flavors = {
  "single" = "6ab6b45e-f5a9-4836-beb5-1efd709e378b"
  "ha"     = "ef0935ba-1335-4a74-9fa5-43ed97786b6a"
}
