# Construction Terraform d'un environnement de base OpenStack Eco

[[_TOC_]]

## Présentation du projet

L'environnement exemple est composé :

* d'un **bastion ssh**
  * exposé sur le réseau interne au travers d'une **ip flottante** ( *fip* )
* d'une **machine virtuelle applicative**
  * exposée sur le réseau interne par l'intermédiaire d'un **répartiteur de charge** (*load balancer*)
  * fournissant un service nginx de base

![Ressources Eco4 du projet](../assets/eco4-101.drawio.png "Ressources Eco4 du projet")

## Pre-requis

Vous devez disposez de [Terraform](https://developer.hashicorp.com/terraform/install),
ou [OpenTofu](https://opentofu.org/docs/intro/install/) avec de faibles ajustements.

### Initialisation de l'environnement de travail

Récuperez depuis l'interface Horizon de Eco4 votre fichier **OpenStack RC**,
il est accessible dans l'interface Horizon depuis votre profil (en haut à droite).

Sourcez ce fichier openrc dans votre environnement virtuel Python afin d'y renseigner les paramètres de votre projet OpenStack (identifiant projet, identifiant et passe du compte, adresses OpenStack, etc.).

*Note : Ce fichier n'est généralement pas nécessaire avec Terraform, ces paramètres peuvent être [renseignés dans Terraform](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs#configuration-reference). Il est ici utilisé pour faciliter le partage entre les exemples cli et Terraform.*

## Création des ressources OpenStack

Référence : [Documentation des ressources du fournisseur Terraform OpenStack](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs)

Le projet Terraform est décomposé en plusieurs scripts :

* [main.tf](main.tf) : point d'entrée
* [variables.tf](variables.tf) : définition des variables, valorisées par défaut pour celles le permettant
* [datasource.tf](datasource.tf) : sources de données Terraform (*data sources*)
* [compute.tf](compute.tf) : ressources liées aux machines virtuelles
* [network.tf](network.tf) : ressources liées au réseau
* [loadbalancer.tf](loadbalancer.tf) : ressources du loadbalancer web

### Le réseau

Il est défini dans le script Terraform [network.tf](network.tf).

Les vm bastion et applicatives sont dans un réseau OpenStack dédié avec son sous-réseau d'adressage.

Positionnez votre sous-réseau dans la classe B 10.0.0.0/16 pour éviter tout conflit avec d'autres réseaux au sein du MTE.
Dans cet exemple le sous réseau est positionné sur classe C 10.0.101.0/24.

Ce réseau doit être raccordé par un routeur au reseau externe SSH `net-2076-ext-ssh` sur la **région 1** de Eco4, `net-egress` sur la **région 2** de Eco 4 ou sur Eco 3.

Le réseau externe SSH est aussi le réseau par défaut.

### Les machines virtuelles

Elles sont définies dans le script Terraform [compute.tf](compute.tf).

Le bastion SSH est le seul point d'entrée pour administrer les machines du projet.
Le serveur applicatif est accessible via ce bastion.

### Le répartiteur de charge

Il est défini dans le script Terraform [loadbalancer.tf](loadbalancer.tf).

Le serveur applicatif doit être exposé sur un réseau externe de OpenStack.

Sur Eco 4 **region 1** le réseau `net-2078-ext-rie` permet de publier hors reverse-proxy sur le réseau interne. Utilisez `net-ingress` sur Eco **région 2** ou sur Eco 3.

*Note : cette étape est la plus complexe mais l'exposition en direct sans répartiteur de charge entrainerait sur Eco d'autres complications de routage au sein des vm.*

*Etape facultative si vous gérez le DNS par ailleurs (ex. WDNS) ou n'en avez pas besoin.*

Les enregistrements DNS sont définis dans les scripts Terraform [compute.tf](compute.tf) et [loadbalancer.tf](loadbalancer.tf).

Si vous avez demandé une zone DNS associée à votre projet OpenStack, vous pouvez créer des enregistrements DNS sur les adresses du bastion et du serveur applicatif.

Les zones DNS allouées sont de type

* xxx.eco4.cloud.e2.rie.gouv.fr : zone publiée sur le RIE interne ministériel

## Exécution du plan Terraform

### Configuration

Utilisez le fichier de variable `eco4r1.tfvars` ou `eco4r2.tfvars`selon que vous utilisiez la région 1 ou la région 2 de Eco 4 respectivement. Modifiez également notamment les premiers paramètres de ce fichier selon votre environnement :

* `vm_sshkeys` : le nom de votre jeu de clés publiques ssh
* `dns_zone_edd`: la zone dns de type eco4.cloud.e2.rie.gouv.fr qui vous a été allouée

[zone_dns] désigne ci-après la zone de type cloud.e2.rie.gouv.fr qui vous a été allouée.

### Exécution

```shell
# Initialisez le projet Terraform (en début de projet)
$ terraform init

# Validez la syntaxe votre plan Terraform
$ terraform validate

# Lancez la planification dans Eco 4 région 2
$ terraform plan --var-file=eco4r2.tfvars

# Appliquez le plan teraform dans Eco 4 région 2
$ terraform apply --var-file=eco4r2.tfvars

# Testez l'adresse de votre bastion
$ ssh debian@bastion.[zone_dns]
# Testez l'adresse de votre serveur web
$ curl http://www.[zone_dns]
...
<title>Welcome to nginx!</title>
...
```

Vous pouvez passer à l'[exemple 102](../102-terraform/README.md) pour aller un peu plus loin avec les **groupes de sécurité**.
