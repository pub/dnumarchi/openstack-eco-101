/*
  Création du load balancer web
*/
resource "openstack_lb_loadbalancer_v2" "lb_web" {
  name           = "${var.lab_id}-lb-web"
  vip_network_id = data.openstack_networking_network_v2.net_ingress_web.id
  flavor_id      = var.lb_flavors["single"]
}

# Création du listener sur le port 80
resource "openstack_lb_listener_v2" "lblistener_web_80" {
  name            = "${var.lab_id}-lblistener-web-80"
  protocol        = "HTTP"
  protocol_port   = 80
  loadbalancer_id = openstack_lb_loadbalancer_v2.lb_web.id
}

# Création du pool de backend
resource "openstack_lb_pool_v2" "lbpool_web_80" {
  name        = "${var.lab_id}-pool-web-80-1"
  protocol    = "HTTP"
  lb_method   = "ROUND_ROBIN"
  listener_id = openstack_lb_listener_v2.lblistener_web_80.id
}

# Ajout du serveur web au pool de backend
resource "openstack_lb_members_v2" "lbmembers_web_80_1" {
  pool_id = openstack_lb_pool_v2.lbpool_web_80.id

  member {
    name          = "${var.lab_id}-web-1"
    address       = openstack_compute_instance_v2.vm_web.access_ip_v4
    protocol_port = 80
    subnet_id     = openstack_networking_subnet_v2.subnet_service_0.id
  }
}

# Création du superviseur de backend
resource "openstack_lb_monitor_v2" "monitor_1" {
  name           = "${var.lab_id}-lbmon-web-80"
  pool_id        = openstack_lb_pool_v2.lbpool_web_80.id
  type           = "HTTP"
  delay          = 20
  timeout        = 10
  max_retries    = 5
  url_path       = "/?health=1"
  expected_codes = 200
    depends_on = [ openstack_lb_members_v2.lbmembers_web_80_1 ]
}

# Création du nom DNS du load balancer
resource "openstack_dns_recordset_v2" "dns_lbweb" {
  zone_id = data.openstack_dns_zone_v2.edd_zone.id
  name    = "www.${var.lab_id}.${data.openstack_dns_zone_v2.edd_zone.name}"
  ttl     = 300
  type    = "A"
  records = [openstack_lb_loadbalancer_v2.lb_web.vip_address]
}

