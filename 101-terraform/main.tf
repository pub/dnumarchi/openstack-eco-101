# Sélection du fournisseur cloud et des versions
terraform {
  required_version = ">= 1.6.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
  }
}

# Configuration du fournisseur OpenStack Terraform
provider "openstack" {
  // Propriétés à déclarer en variables ou sourcer fichier OpenStack openrc
  // https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs
}

/*
  Les sorties  DNS informatives
*/
output "DNS-Bastion" {
  value = openstack_dns_recordset_v2.dns_vmbastion.name
}

output "DNS-Web" {
  value = openstack_dns_recordset_v2.dns_lbweb.name
}
