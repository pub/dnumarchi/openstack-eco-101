/*
 Création de la VM Bastion
*/
#   => Le port IP du bastion sur son réseau
#     => nécessaire pour y associer une ip flottante
resource "openstack_networking_port_v2" "port_bastion" {
  name           = "${var.lab_id}-port-${var.vm_bastion_props.name}"
  network_id     = openstack_networking_network_v2.net_service.id
  admin_state_up = "true"

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.subnet_service_0.id
    ip_address = var.vm_bastion_props.ipv4
  }
}

#   => la VM bastion sur le port précédent
resource "openstack_compute_instance_v2" "vm_bastion" {
  name        = "${var.lab_id}-${var.vm_bastion_props.name}"
  image_name  = var.vm_bastion_props.image
  flavor_name = var.vm_bastion_props.flavor
  key_pair    = var.vm_sshkeys
  network {
    name = openstack_networking_network_v2.net_service.name
    port = openstack_networking_port_v2.port_bastion.id
  }
}

#   => Demande d'IP flottante pour le bastion
resource "openstack_networking_floatingip_v2" "fip_bastion" {
  pool        = data.openstack_networking_network_v2.net_egress.name
  description = "${var.lab_id} fip bastion"
}

#   => Affectation de l'ip flottante au bastion
resource "openstack_networking_floatingip_associate_v2" "fip_bastion_add" {
  floating_ip = openstack_networking_floatingip_v2.fip_bastion.address
  port_id     = openstack_networking_port_v2.port_bastion.id
  depends_on  = [openstack_networking_router_interface_v2.router_service]
}

# Création du nom DNS du bastion
resource "openstack_dns_recordset_v2" "dns_vmbastion" {
  zone_id = data.openstack_dns_zone_v2.edd_zone.id
  name    = "bastion.${var.lab_id}.${data.openstack_dns_zone_v2.edd_zone.name}"
  ttl     = 120
  type    = "A"
  records = [openstack_networking_floatingip_v2.fip_bastion.address]
}

/*
 Création de la VM web
*/
resource "openstack_compute_instance_v2" "vm_web" {
  name        = "${var.lab_id}-${var.vm_web_props.name}"
  image_name  = var.vm_web_props.image
  flavor_name = var.vm_web_props.flavor
  key_pair    = var.vm_sshkeys
  network {
    name        = openstack_networking_network_v2.net_service.name
    fixed_ip_v4 = var.vm_web_props.ipv4 # Pas de fip, affectation directe sans port identifié
  }
  user_data  = file("vmapp-init.sh") # Installation du serveur nginx
  depends_on = [openstack_networking_subnet_v2.subnet_service_0]
}

