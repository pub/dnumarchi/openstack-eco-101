# Création du réseau privé
resource "openstack_networking_network_v2" "net_service" {
  name           = "${var.lab_id}-net"
  admin_state_up = true
}

# Création du sous-réseau d'adressage
resource "openstack_networking_subnet_v2" "subnet_service_0" {
  name            = "${var.lab_id}-subnet0"
  network_id      = openstack_networking_network_v2.net_service.id
  cidr            = var.subnet_cidr
  ip_version      = 4
  enable_dhcp     = true
  dns_nameservers = var.dns_servers
}

# Création du routeur et son interface externe
resource "openstack_networking_router_v2" "ext_router" {
  name                = "${var.lab_id}-router"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.net_egress.id
}

# Affectation d'une adresse au routeur sur le réseau privé
resource "openstack_networking_port_v2" "port_router" {
  name       = "${var.lab_id}-port-router"
  network_id = openstack_networking_network_v2.net_service.id
  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.subnet_service_0.id
    ip_address = var.ip_router_internal
  }
}

# Attachement du routeur au réseau privé
resource "openstack_networking_router_interface_v2" "router_service" {
  router_id = openstack_networking_router_v2.ext_router.id
  port_id   = openstack_networking_port_v2.port_router.id
}
