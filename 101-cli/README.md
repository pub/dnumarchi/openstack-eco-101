# Construction par le client OpenStack d'un environnement OpenStack Eco

[[_TOC_]]

## Présentation du projet

L'environnement exemple est composé :

* d'un **bastion ssh**
  * exposé sur le réseau interne au travers d'une **ip flottante** ( *fip* )
* d'une **machine virtuelle applicative**
  * exposée sur le réseau interne par l'intermédiaire d'un **répartiteur de charge** (*load balancer*)
  * fournissant un service nginx de base

![Ressources Eco4 du projet](../assets/eco4-101.drawio.png "Ressources Eco4 du projet")

## Pre-requis

En complément aux [pré-requis présentés en introduction](../README.md#pre-requis).

Les commandes ci-dessous sont conçues pour un système **Linux**. Elles sont adaptables à un système Microsoft Windows sans trop de changements, toute contribution à ce projet pour l'illustrer sera la bienvenue.

### Interpéteur Python

Vous devez disposer d'un [interpréteur Python 3](https://docs.python.org/fr/3/using/index.html).

### Client OpenStack

Les paquets Python à installer :

* `python-openstackclient` : client OpenStack
* `python-octaviaclient` : extension Octavia pour gestion des répartiteurs de charge
* `python-designateclient` : extension pour gestion des zones DNS

Créez un **environnement virtuel Python** et utilisez le dès que vous devez accéder à votre environnement Eco4.

```shell
# Créez un environnement virtuel Python
$ python3 -m venv venv_ostack
# Activez cet environnement virtuel
$ . venv_ostack/bin/activate
# Installez le client Openstack
$ pip install python-openstackclient
$ pip install python-octaviaclient
$ pip install python-designateclient
# Vérifiez l'installation
$ openstack --version
openstack 6.5.0
```

Références :

* https://pypi.org/project/python-openstackclient/
* https://docs.openstack.org/python-octaviaclient/latest/install/index.html

### Initialisation de l'environnement de travail

Récuperez depuis l'interface Horizon de Eco4 votre fichier **OpenStack RC**,
il est accessible dans l'interface Horizon depuis votre profil (en haut à droite).

Sourcez ce fichier openrc dans votre environnement virtuel Python afin d'y renseigner les paramètres de votre projet OpenStack (identifiant projet, identifiant et passe du compte, adresses OpenStack, etc.).

```shell
# Initialisez vos variables d'environnement OpenStack
$ . monprojet-openrc.sh
Please enter your OpenStack Password for project monprojet as user mon.id:

# Vérifiez les paramètres de connexion en listant les images du projet OpenStack
$ openstack image list
+--------------------------------------+------------------+--------+
| ID                                   | Name             | Status |
+--------------------------------------+------------------+--------+
| 58ed66e8-c054-4dc8-a825-5b0a7592d982 | Debian 10        | active |
| 8e79f8c8-16ad-4ae1-b31a-b967fad741df | Debian 11        | active |
| 9d54665b-c9be-4ebc-a5c0-3003234c764e | Debian 12        | active |
...
```

Références :

* https://docs.openstack.org/newton/user-guide/common/cli-set-environment-variables-using-openstack-rc.html

## Création des ressources OpenStack

Ci-après les étapes de créations de l'environnement.

Un [script global](lab101.sh) reprend l'ensemble de ces commandes. Modifiez comme de besoin les variables en tête de script, notamment `DNS_ZONE` et `KEYS_NAME`.

### Le réseau

Les vm bastion et applicative sont dans un réseau OpenStack dédié avec son sous-réseau d'adressage.

Positionnez votre sous-réseau dans la classe B 10.0.0.0/16 pour éviter tout conflit avec d'autres réseaux au sein du MTE.
Dans cet exemple le sous réseau est positionné sur classe C 10.0.101.0/24.

```shell
# Créez le réseau projet, notez l'identifiant réseau (id), il servira par la suite
# et sera noté [net_id]
#   - [net_id] = bfd6b325... dans l'exemple ci-après
$ openstack network create lab101-net
+---------------------------+--------------------------------------+
| Field                     | Value                                |
+---------------------------+--------------------------------------+
| admin_state_up            | UP                                   |
...
| id                        | bfd6b325-79ed-4889-8dd5-2e2a58f132c0 |
| ipv4_address_scope        | None                                 |
...

# Créez le sous-réseau projet, notez l'identifiant sous-réseau (id), il servira par la suite
# et sera noté [subnet_id].
#   - [subnet_id] = 5da08d2c dans l'exemple ci-après
# Serveur DNS utilisé : 10.167.130.2
$ openstack subnet create --network lab101-net --dhcp --dns-nameserver 10.167.130.2 \
      --subnet-range 10.0.101.0/24 lab10-subnet
+----------------------+--------------------------------------+
| Field                | Value                                |
+----------------------+--------------------------------------+
| allocation_pools     | 10.0.101.2-10.0.101.254        |
| cidr                 | 10.0.101.0/24                     |
...
| id                   | 5da08d2c-35d3-4800-a9e0-c272240bc922 |
| ip_version           | 4                                    |
...
```

Ce réseau doit être raccordé par un routeur au reseau externe SSH `net-2076-ext-ssh` sur la **région 1** de Eco4, `net-egress` sur la **région 2** de Eco 4 ou sur Eco 3.

Le réseau externe SSH est aussi le réseau par défaut.

```shell
# Créez le routeur
$ openstack router create lab101-router
# Attachez le routeur au réseau externe ssh
$ openstack router set --external-gateway net-2076-ext-ssh lab101-router
# Attachez le routeur au réseau privé du projet
$ openstack router add subnet lab101-router lab10-subnet
```

Références :

* https://docs.openstack.org/python-openstackclient/latest/cli/command-objects/subnet.html
* https://docs.openstack.org/python-openstackclient/latest/cli/command-objects/network.html

### Les machines virtuelles

Le bastion SSH est le seul point d'entrée pour administrer les machines du projet.
Le serveur applicatif est accessible via ce bastion.

#### Le bastion ssh

Une ip flottante est associée à ce bastion sur le réseau externe ssh. 

Note : adaptez le nom du réseau externe comme indiqué précédemment.

```shell
# Créez le bastion ssh
# [lab101-keys] désigne l'identifiant de votre jeu de clés SSH précédemment importé dans le projet
$ openstack server create --image "Debian 12" --flavor m1.tiny \
    --network lab101-net --key-name [lab101-keys] lab101-bastion --wait

# Créez une ip flottante, notez l'adresse allouée (floating_ip_address), elle servir par la suite
# et sera notée [fip_adresse].
#   - [fip_address] = 10.165.77.86 dans l'exemple ci-dessous
$ openstack floating ip create --description "IP Bastion lab 101" --tag lab101 net-2076-ext-ssh
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
...
| floating_ip_address | 10.165.77.86                         |
...

# Associez l'ip flottante créée au bastion
$ openstack server add floating ip lab101-bastion [fip_address]
# Testez l'accès au bastion
$ ssh debian@[fip_address]
```

#### Le serveur applicatif

Le serveur applicatif gère un service nginx.
Dans la pratique, les installations systèmes sont automatisables par Ansible.
Dans cet exemple, pour simplifier, le service nginx est installé par un [script](app-init.sh) en *cloud-init*.


```shell
# Créez le serveur applicatif
#   - Une ip fixe interne pour le load balancer qui sera ensuite créé, 10.0.101.20 dans l'exemple
#   - [net_id] est l'identifiant réseau créé précédemment
$ openstack server create --image "Debian 12" --flavor m1.small \
    --key-name [lab101-keys] --nic "net-id=[net_id],v4-fixed-ip=10.0.101.20" \
    --user-data vmapp-init.sh --wait lab101-app
```

Références :

* https://docs.openstack.org/python-openstackclient/latest/cli/command-objects/server.html
* https://docs.openstack.org/python-openstackclient/latest/cli/command-objects/floating-ip.html

### Le répartiteur de charge (LB - *Load balancer*)

Le serveur applicatif doit être exposé sur un réseau externe de OpenStack.

Sur Eco 4 **region 1** le réseau `net-2078-ext-rie` permet de publier hors reverse-proxy sur le réseau interne. Utilisez `net-ingress` sur Eco **région 2** ou sur Eco 3.

*Note : cette étape est la plus complexe mais l'exposition en direct sans répartiteur de charge entrainerait sur Eco d'autres complications de routage au sein des vm.*

```shell
# Créez le load balancer exposé sur net-2078-ext-rie. Notez pour la suite
# l'adresse d'exposition allouée, notée [vip_address].
#   - [vip_address] = 10.165.78.88 dans l'exemple ci-dessous
$ openstack loadbalancer create  --name lab101-lb --vip-network-id net-2078-ext-rie
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
...
| vip_address         | 10.165.78.88                         |
...

# Créez un listener sur le port 80
$ openstack loadbalancer listener create --name lab101-lblst --protocol HTTP --protocol-port 80 lab101-lb
# Créez un pool de ressources backend
$ openstack loadbalancer pool create --name lab101-lbpool \
    --protocol HTTP --listener lab101-lblst --lb-algorithm LEAST_CONNECTIONS
# Créez un superviseur des serveurs backend
$ openstack loadbalancer healthmonitor create --type HTTP --http-method GET \
      --expected-codes 200 --name lab101-lbhealth \
      --delay 5 --timeout 5 --max-retries 3 lab101-lbpool
# Ajoutez le serveur applicatif au load balancer
#   - [subnet_id] est l'identifiant du sous-réseau projet
$ openstack loadbalancer member create --name lab101-lbmember1 --protocol-port 80 \
    --address 10.0.101.20 --subnet-id=[subnet_id] lab101-lbpool

# Testez le service nginx sur l'adresse exposée par le load balancer
$ curl http://[vip_address]
...
<title>Welcome to nginx!</title>
...
```
Références :

* https://docs.openstack.org/python-octaviaclient/latest/cli/index.html
* https://docs.openstack.org/octavia/latest/user/guides/basic-cookbook.html

### Le DNS

*Etape facultative si vous gérez le DNS par ailleurs (ex. WDNS) ou n'en avez pas besoin.*

Si vous avez demandé une zone DNS associée à votre projet OpenStack, vous pouvez créer des enregistrements DNS sur les adresses du bastion et du serveur applicatif.

Les zones DNS allouées sont de type

* xxx.eco4.cloud.e2.rie.gouv.fr : zone publiée sur le RIE interne ministériel
* xxx.eco4.sihc.fr : zone interne au tenant

[zone_dns] désigne ci-après la zone de type cloud.e2.rie.gouv.fr qui vous a été allouée.

```shell
# Recherchez l'identifiant de la zone DNS allouée
$ zone_id=$(openstack zone list --name [zone_dns] -f value -c id)
# Enregistrez votre bastion dans le DNS, TTL à 2' pour les besoins de l'exemple.
#   - [fip_address] : adresse ip flottante du bastion précédemment récupérée
$ openstack recordset create --type A --ttl 120 --record [fip_address] "$zone_id" bastion
# Enregistrez votre serveur web dans le DNS, TTL à 2' pour les besoins de l'exemple.
# - [vip_address] : adresse IP load balancer précédemment récupérée
$ openstack recordset create --type A --ttl 120  --record [vip_address] "$zone_id" www

# Testez l'adresse de votre bastion
$ ssh debian@bastion.[zone_dns]
# Testez l'adresse de votre serveur web
$ curl http://www.[zone_dns]
...
<title>Welcome to nginx!</title>
...
```

Références :

* https://docs.openstack.org/python-designateclient/
