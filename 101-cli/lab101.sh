#!/bin/sh
set -x

#####################################################################
# Creation d'un environnement de base Eco4
#####################################################################

# Nom du projet, sert à préfixer les ressources
PJ_NAME=lab101
# CIDR du sous-réseau dédié au projet
NET_SUB_CIDR=10.0.101.0/24
# Adresse IP interne de la VM applicative
VM_APP_IP=10.0.101.20
# Serveur DNS
NET_DNS=10.167.130.2
# Réseau externe ssh et sortant
NET_EXT_SSH=net-2076-ext-ssh  # Eco4 région 1
#NET_EXT_SSH=net-egress       # Eco4 région 2, Eco3
# Réseau externe de publication web
NET_EXT_RIE=net-2078-ext-rie  # Eco4 région 1
#NET_EXT_RIE=net-ingress      # Eco4 région 2, Eco3
# Zone DNS RIE allouée au projet
DNS_ZONE=mon-projet.eco4.cloud.e2.rie.gouv.fr.      # Eco4 région 1
#DNS_ZONE=mon-projet.r2.eco4.cloud.e2.rie.gouv.fr.  # Eco4 région 2
#DNS_ZONE=mon-projet.eco3.cloud.e2.rie.gouv.fr.     # Eco 3
# Jeu de clés ssh administrateur
KEYS_NAME=my-keys

# Les paramètres suivant sont calculés
NET_NAME=$PJ_NAME-net             # Nom du réseau projet
NET_SUB_NAME=$PJ_NAME-sub         # Nom du sous-réseau projet
ROUTER_NAME=$PJ_NAME-router       # Nom du routeur de rattachement au réseau ssh
VM_BASTION_NAME=$PJ_NAME-bastion  # Nom du bastion
VM_APP_NAME=$PJ_NAME-app          # Nom du serveur applicatif

# Construction des ressources de l'environnement OpenStack
env_build() {
  # Réseau projet
  net_id=$(openstack network create $NET_NAME -f value -c id)
  subnet_id=$(openstack subnet create --network $NET_NAME --dhcp --dns-nameserver $NET_DNS \
      --subnet-range $NET_SUB_CIDR -f value -c id $NET_SUB_NAME)
  
  # Interconnexion réseau ssh rie/edd
  openstack router create $ROUTER_NAME
  openstack router set --external-gateway $NET_EXT_SSH $ROUTER_NAME
  openstack router add subnet $ROUTER_NAME $NET_SUB_NAME

  # Création de la VM bastion
  openstack server create --image "Debian 12" --flavor m1.tiny \
      --network $NET_NAME --key-name $KEYS_NAME $VM_BASTION_NAME --wait
  # Ajout de l'ip flottante au bastion
  fip_bastion=$(openstack floating ip create \
      --description "IP Bastion $PJ_NAME" --tag $PJ_NAME \
      -c floating_ip_address -f value $NET_EXT_SSH)
  openstack server add floating ip $VM_BASTION_NAME "$fip_bastion"

  # Creation de la VM applicative, IP fixe pour Load Balancer
  openstack server create --image "Debian 12" --flavor m1.small \
    --key-name $KEYS_NAME $VM_APP_NAME \
    --nic "net-id=$net_id,v4-fixed-ip=$VM_APP_IP" \
    --user-data vmapp-init.sh --wait

  # Creation du load balancer pour accès web
  fip_lb=$(openstack loadbalancer create  --wait --name $PJ_NAME-lb\
      --vip-network-id $NET_EXT_RIE -f value -c vip_address)
  openstack loadbalancer listener create  --wait --name $PJ_NAME-lblst \
      --protocol HTTP --protocol-port 80 $PJ_NAME-lb
  openstack loadbalancer pool create --wait --name $PJ_NAME-lbpool --protocol HTTP \
      --listener $PJ_NAME-lblst --lb-algorithm LEAST_CONNECTIONS
  openstack loadbalancer healthmonitor create --type HTTP --http-method GET \
      --expected-codes 200 --name $PJ_NAME-lbhealth \
      --delay 5 --timeout 5 --max-retries 3 $PJ_NAME-lbpool
  openstack loadbalancer member create --wait --name $PJ_NAME-lbmember1 \
      --address "$VM_APP_IP" "--subnet-id=$subnet_id" --protocol-port 80 \
      $PJ_NAME-lbpool

  # Création des enregistrements DNS du bastion et du serveur web
  if [ -n "$DNS_ZONE" ]; then
    # Recherche de l'identifiant de la zone DNS
    zone_id=$(openstack zone list --name "$DNS_ZONE" -f value -c id)
    # Enregistrement DNS du bastion
    openstack recordset create --type A --ttl 120 --record "$fip_bastion" "$zone_id" bastion.$PJ_NAME
    # Enregistrement DNS du serveur web
    openstack recordset create --type A --ttl 120 --record "$fip_lb" "$zone_id" www.$PJ_NAME
  fi

  # Informations sur l'environnement
  echo "Bastion : dns=bastion.$PJ_NAME.$DNS_ZONE / ip=$fip_bastion"
  echo "Serveur Web : dns=www.$PJ_NAME.$DNS_ZONE / ip=$fip_lb"
}

# Suppression des ressources de l'environnement OpenStack
env_destroy() {
  # Suppression des enregistrements DNS
  if [ -n "$DNS_ZONE" ]; then
    # Recherche de l'identifiant de la zone DNS
    zone_id=$(openstack zone list --name "$DNS_ZONE" -f value -c id)
    dns_bastion_id=$(openstack recordset list -f value -c id --name "bastion.$PJ_NAME.$DNS_ZONE"  "$zone_id")
    [ -n "$dns_bastion_id" ] && openstack recordset delete "$zone_id" "$dns_bastion_id"
    dns_www_id=$(openstack recordset list -f value -c id --name "www.$PJ_NAME.$DNS_ZONE" "$zone_id")
    [ -n "$dns_www_id" ] && openstack recordset delete "$zone_id" "$dns_www_id"
  fi

  # Suppression de la FIP du bastion
  fip_bastion=$(openstack floating ip list --tag $PJ_NAME -c "Floating IP Address" -f value)
  openstack floating ip delete "$fip_bastion"

  # Suppression du load balancer
  openstack loadbalancer member delete $PJ_NAME-lbpool $PJ_NAME-lbmember1 --wait
  openstack loadbalancer pool delete $PJ_NAME-lbpool --wait
  openstack loadbalancer listener delete $PJ_NAME-lblst --wait
  openstack loadbalancer delete $PJ_NAME-lb --wait

  # Suppression des VM
  openstack server delete $VM_APP_NAME --wait
  openstack server delete $VM_BASTION_NAME --wait

  # Déconnexion du routeur de ses interface puis suppression
  openstack router unset --external-gateway $ROUTER_NAME
  openstack router remove subnet $ROUTER_NAME $NET_SUB_NAME
  openstack router delete $ROUTER_NAME

  # Suppression des réseaux
  openstack network delete $NET_NAME
}

# Point d'entrée principal
ACTION=$1
case $ACTION in
  "build")
    env_build
    ;;
  "destroy")
    env_destroy
    ;;
  *)
    echo "Usage $0 build|destroy"
esac
