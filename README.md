# Construction d'un environnement OpenStack Eco

[[_TOC_]]

Exemple de construction d'un environnement OpenStack sur le cloud Eco du MTE.

## Pre-requis

Vous devez disposer d'un **projet OpenStack** sur le cloud Eco du MTE.

## Jeu de clés SSH

Les clés SSH permettent d'accéder à vos machines virtuelles. Vous devez les définir dans votre projet OpenStack via des *KeyPair* :

* au travers des [API OpenStack](https://docs.openstack.org/python-openstackclient/latest/cli/command-objects/keypair.html
)
* ou au travers de l'interface Horizon  : *Compute* / *Paire de clés*

Dans les exemples CLI et Terraform l'identifiant de votre jeu de clés SSH sera représenté par **[labxxx-keys]**.

**Note** : vous pouvez aussi utilisez les clés issues de votre carte agent, non abordées ici pour ne pas complexifier l'exemple.

## Construction de l'environnement par le client OpenStack (CLI)

Reportez vous à l'exemple [101-cli](101-cli/README.md).

## Construction de l'environnement par Terraform

### Environnement de base

Reportez vous à l'exemple [101-terraform](101-terraform/README.md) pour l'équivalent de *101-cli*.

### Environnement avec security groups

L'exemple [102-terraform](102-terraform/README.md) ajoute des *Security groups*.
