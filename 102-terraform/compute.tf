/*
 Création de la VM Bastion
*/
#   => Le port IP du bastion sur son réseau
#     => nécessaire pour ip flottante
#   => SecurityGroup sur le port lorsque rattachement par port (doc provider)
resource "openstack_networking_port_v2" "port_bastion" {
  name           = "${var.lab_id}-port-${var.vm_bastion_props.name}"
  network_id     = openstack_networking_network_v2.net_service.id
  admin_state_up = "true"
  security_group_ids = [openstack_networking_secgroup_v2.sg_support.id,
  openstack_networking_secgroup_v2.sg_bastion.id]
  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.subnet_service_0.id
    ip_address = var.vm_bastion_props.ipv4
  }
}

#   => la VM bation sur le port précédent
resource "openstack_compute_instance_v2" "vm_bastion" {
  name        = "${var.lab_id}-${var.vm_bastion_props.name}"
  image_name  = var.vm_bastion_props.image
  flavor_name = var.vm_bastion_props.flavor
  key_pair    = var.vm_sshkeys
  network {
    name = openstack_networking_network_v2.net_service.name
    port = openstack_networking_port_v2.port_bastion.id
  }
}

#   => Demande d'IP flottante pour bastion
resource "openstack_networking_floatingip_v2" "fip_bastion" {
  pool        = data.openstack_networking_network_v2.net_egress.name
  description = "${var.lab_id} fip bastion"
}

#   => Affectation de l'ip flottante au bastion
resource "openstack_networking_floatingip_associate_v2" "fip_bastion_add" {
  floating_ip = openstack_networking_floatingip_v2.fip_bastion.address
  port_id     = openstack_networking_port_v2.port_bastion.id
  depends_on  = [openstack_networking_router_interface_v2.router_service]
}

# Création du nom DNS du bastion
resource "openstack_dns_recordset_v2" "dns_vmbastion" {
  zone_id = data.openstack_dns_zone_v2.edd_zone.id
  name    = "bastion.${var.lab_id}.${data.openstack_dns_zone_v2.edd_zone.name}"
  ttl     = 120
  type    = "A"
  records = [openstack_networking_floatingip_v2.fip_bastion.address]
}

/*
 Création de la VM outils
*/
resource "openstack_compute_instance_v2" "vm_tools" {
  name        = "${var.lab_id}-${var.vm_tools_props.name}"
  image_name  = var.vm_tools_props.image
  flavor_name = var.vm_tools_props.flavor
  key_pair    = var.vm_sshkeys
  security_groups = [openstack_networking_secgroup_v2.sg_support.name,
  openstack_networking_secgroup_v2.sg_apps.name]
  network {
    name        = openstack_networking_network_v2.net_service.name
    fixed_ip_v4 = var.vm_tools_props.ipv4 #  Pas de fip, affectation directe sans port identifié
  }
  depends_on = [openstack_networking_subnet_v2.subnet_service_0]
}

# Nom dns interne VM outils
resource "openstack_dns_recordset_v2" "dns_vmtools" {
  zone_id = data.openstack_dns_zone_v2.sihc_zone.id
  name    = "${openstack_compute_instance_v2.vm_tools.name}.${var.lab_id}.${data.openstack_dns_zone_v2.sihc_zone.name}"
  ttl     = 300
  type    = "A"
  records = [openstack_compute_instance_v2.vm_tools.access_ip_v4]
}

/*
 Création de la VM web
*/
resource "openstack_compute_instance_v2" "vm_web" {
  name        = "${var.lab_id}-${var.vm_web_props.name}"
  image_name  = var.vm_web_props.image
  flavor_name = var.vm_web_props.flavor
  key_pair    = var.vm_sshkeys
  security_groups = [openstack_networking_secgroup_v2.sg_support.name,
    openstack_networking_secgroup_v2.sg_apps.name,
  openstack_networking_secgroup_v2.sg_app_web.name]
  network {
    name        = openstack_networking_network_v2.net_service.name
    fixed_ip_v4 = var.vm_web_props.ipv4 # Pas de fip, affectation directe sans port identifié
  }
  user_data  = file("vmweb-init.sh") # Installation du serveur nginx
  depends_on = [openstack_networking_subnet_v2.subnet_service_0]
}

# Nom dns interne VM Web
resource "openstack_dns_recordset_v2" "dns_vmweb" {
  zone_id = data.openstack_dns_zone_v2.sihc_zone.id
  name    = "${openstack_compute_instance_v2.vm_web.name}.${var.lab_id}.${data.openstack_dns_zone_v2.sihc_zone.name}"
  ttl     = 300
  type    = "A"
  records = [openstack_compute_instance_v2.vm_web.access_ip_v4]
}

/*
 Création de la VM applicative
*/
resource "openstack_compute_instance_v2" "vm_app" {
  name        = "${var.lab_id}-${var.vm_app_props.name}"
  image_name  = var.vm_app_props.image
  flavor_name = var.vm_app_props.flavor
  key_pair    = var.vm_sshkeys
  security_groups = [openstack_networking_secgroup_v2.sg_support.name,
  openstack_networking_secgroup_v2.sg_apps.name]
  network {
    name        = openstack_networking_network_v2.net_service.name
    fixed_ip_v4 = var.vm_app_props.ipv4 # Pas de fip, affectation directe sans port identifié
  }
  depends_on = [openstack_networking_subnet_v2.subnet_service_0]
}

# Nom dns interne VM applicative
resource "openstack_dns_recordset_v2" "dns_vmapp" {
  zone_id = data.openstack_dns_zone_v2.sihc_zone.id
  name    = "${openstack_compute_instance_v2.vm_app.name}.${var.lab_id}.${data.openstack_dns_zone_v2.sihc_zone.name}"
  ttl     = 300
  type    = "A"
  records = [openstack_compute_instance_v2.vm_app.access_ip_v4]
}

/*
 Création de la VM base de données
*/
resource "openstack_compute_instance_v2" "vm_pg" {
  name        = "${var.lab_id}-${var.vm_pg_props.name}"
  image_name  = var.vm_pg_props.image
  flavor_name = var.vm_pg_props.flavor
  key_pair    = var.vm_sshkeys
  security_groups = [openstack_networking_secgroup_v2.sg_support.name,
    openstack_networking_secgroup_v2.sg_apps.name,
  openstack_networking_secgroup_v2.sg_app_pg.name]
  network {
    name        = openstack_networking_network_v2.net_service.name
    fixed_ip_v4 = var.vm_pg_props.ipv4 # Pas de fip, affectation directe sans port identifié
  }
  depends_on = [openstack_networking_subnet_v2.subnet_service_0]
}

# Nom dns interne VM base de données
resource "openstack_dns_recordset_v2" "dns_vmpg" {
  zone_id = data.openstack_dns_zone_v2.sihc_zone.id
  name    = "${openstack_compute_instance_v2.vm_pg.name}.${var.lab_id}.${data.openstack_dns_zone_v2.sihc_zone.name}"
  ttl     = 300
  type    = "A"
  records = [openstack_compute_instance_v2.vm_pg.access_ip_v4]
}
