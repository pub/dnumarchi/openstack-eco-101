# =============================
# Variables pour Eco4 Région 1
# =============================

# Jeu de clés publiques ssh
vm_sshkeys = "my-keys"
# Zone DNS externe allouée
dns_zone_edd = "mon-projet.eco4.cloud.e2.rie.gouv.fr."
# Zone DNS interne allouée
dns_zone_sihc = "mon-projet.eco4.sihc.fr."

# Identifiant du projet, préfixe des ressources
lab_id = "lab102"
# Réseau externe en sortie
net_egress = "net-2076-ext-ssh"
# Réseau externe en entrée web
net_ingress = "net-2078-ext-rie"

# Identifiants des gabarits de load balancer
lb_flavors = {
  "single" = "3cbed89a-ae3f-412d-861e-c304307fbad9"
  "ha"     = "311a6d31-a3f7-4228-87ec-63102b926b90"
}
