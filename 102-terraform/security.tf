/*
  Security group pour règles entre toutes les vm du projet
*/
resource "openstack_networking_secgroup_v2" "sg_support" {
  name                 = "${var.lab_id}-sg-support"
  description          = "Flux support entre VM du projet"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_rule_v2" "sgr_support_icmp_in" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_group_id   = openstack_networking_secgroup_v2.sg_support.id
  security_group_id = openstack_networking_secgroup_v2.sg_support.id
}

resource "openstack_networking_secgroup_rule_v2" "sgr_support_udp_in" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_group_id   = openstack_networking_secgroup_v2.sg_support.id
  security_group_id = openstack_networking_secgroup_v2.sg_support.id
}

resource "openstack_networking_secgroup_rule_v2" "sgr_support_out" {
  direction         = "egress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sg_support.id
}

/*
  Security group pour le bastion
*/
resource "openstack_networking_secgroup_v2" "sg_bastion" {
  name                 = "${var.lab_id}-sg-bastion"
  description          = "Pour bastion ssh"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_rule_v2" "sgr_bastion_in_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  security_group_id = openstack_networking_secgroup_v2.sg_bastion.id
}

/*
  Security group pour la vm applicative web exposée sur le réseau externe
*/
resource "openstack_networking_secgroup_v2" "sg_app_web" {
  name                 = "${var.lab_id}-sg-app-web"
  description          = "VM applicatives Web"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_rule_v2" "sgr_app_in_web" {
  for_each          = toset(["80", "443"])
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = each.key
  port_range_max    = each.key
  security_group_id = openstack_networking_secgroup_v2.sg_app_web.id
}

/*
  Security group pour  la vm applicative Postgresq si exposée sur un réseau externe
*/
resource "openstack_networking_secgroup_v2" "sg_app_pg" {
  name                 = "${var.lab_id}-sg-app-pg"
  description          = "VM applicatives Postgresql"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_rule_v2" "sgr_app_in_pg" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  security_group_id = openstack_networking_secgroup_v2.sg_app_pg.id
}

/*
  Security group commun aux vm applicatives
*/
resource "openstack_networking_secgroup_v2" "sg_apps" {
  name                 = "${var.lab_id}-sg-apps"
  description          = "Flux communs VM applicatives"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_rule_v2" "sgr_apps_all_internal" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_group_id   = openstack_networking_secgroup_v2.sg_apps.id
  security_group_id = openstack_networking_secgroup_v2.sg_apps.id
}

resource "openstack_networking_secgroup_rule_v2" "sgr_apps_bastion_in" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_group_id   = openstack_networking_secgroup_v2.sg_bastion.id
  security_group_id = openstack_networking_secgroup_v2.sg_apps.id
}
