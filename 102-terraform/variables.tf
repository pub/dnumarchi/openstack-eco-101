/*
  Variables projet
*/
variable "lab_id" {
  description = "Identifiant du projet, préfixe des ressources"
  type        = string
  nullable    = false
}

/*
  Variables machines virtuelles
*/
variable "vm_image" {
  description = "Image du système d'exploitation"
  type        = string
  default     = "Debian 12"
}

variable "vm_bastion_props" {
  description = "Propriétés de la VM bastion"
  type        = map(string)
  default = {
    name   = "bastion"
    image  = "Debian 12"
    flavor = "m1.tiny"
    ipv4   = "10.0.102.10"
  }
}

variable "vm_tools_props" {
  description = "Propriétés de la VM outils"
  type        = map(string)
  default = {
    name   = "tools"
    image  = "Debian 12"
    flavor = "m1.small"
    ipv4   = "10.0.102.11"
  }
}

variable "vm_web_props" {
  description = "Propriétés de la VM web"
  type        = map(string)
  default = {
    name   = "web"
    image  = "Debian 12"
    flavor = "m1.small"
    ipv4   = "10.0.102.20"
  }
}

variable "vm_app_props" {
  description = "Propriétés de la VM applicative"
  type        = map(string)
  default = {
    name   = "app"
    image  = "Debian 12"
    flavor = "m1.small"
    ipv4   = "10.0.102.21"
  }
}

variable "vm_pg_props" {
  description = "Propriétés de la VM Postgresql"
  type        = map(string)
  default = {
    name   = "db"
    image  = "Debian 12"
    flavor = "m1.small"
    ipv4   = "10.0.102.22"
  }
}

/*
  Variables sécurité
*/
variable "vm_sshkeys" {
  description = "Jeu de clé SSH des VM"
  type        = string
  nullable    = false
}

/*
  Variables réseau
*/
variable "net_egress" {
  description = "Nom du réseau externe egress / sortie"
  type        = string
  default     = "net-2076-ext-ssh"
}

variable "net_ingress" {
  description = "Nom du réseau externe ingress / entrée web"
  type        = string
  default     = "net-2078-ext-rie"
}

variable "subnet_cidr" {
  description = "Masque CIDR du sous-réseau"
  type        = string
  default     = "10.0.102.0/24"
}

variable "ip_router_internal" {
  description = "Adresse IP du routeur dans le projet"
  type        = string
  default     = "10.0.102.1"
}

variable "dns_servers" {
  description = "Liste de serveurs DNS"
  type        = list(string)
  default     = ["10.167.130.2"]
}

variable "dns_zone_edd" {
  description = "Nom de la zone DNS exposée allouée au projet"
  type        = string
}

variable "dns_zone_sihc" {
  description = "Nom de la zone DNS privée allouée au projet"
  type        = string
}

/*
  Variables répartiteurs de charge
*/
variable "lb_flavors" {
  description = "Gabarits des LB"
  type        = map(string)
}
